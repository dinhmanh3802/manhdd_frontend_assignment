$(document).ready(function () {
    showContents()
})

function showContents() {
    var userInfos = JSON.parse(getCookie('userInfos'))
    var tbody = $("#userList tbody");
    var count = 0
    for (const userInfo of userInfos) {
        var row = "<tr><td>" + ++count + "</td><td>" + userInfo.firstName + "</td><td>" + userInfo.lastName + "</td><td>" + userInfo.email + "</td><td>" + userInfo.userName + "</td></tr>";
        tbody.append(row);
    }

}






$('#form-user').submit(function (e) {

    var errorMessage = ""

    var firstNameElement = $("#inputFirstName").val();
    var lastNameElement = $("#inputLastName").val();
    var emailElement = $("#inputEmail").val();
    var userNameElement = $("#inputUserName").val();
    var passwordElement = $("#inputPassword").val();
    var confirmPasswordElement = $("#inputConfirmPassword").val();

    if (firstNameElement == "" || lastNameElement == "" || userNameElement == "" || passwordElement == "" || confirmPasswordElement == "") {
        errorMessage += "Please fill all mandatory fields<br>";
    }
    if (firstNameElement == "") {
        $("#inputFirstName").css("border-color", "red");
    } else {
        $("#inputFirstName").css("border-color", "rgb(0, 198, 0)");
    }
    if (lastNameElement == "") {
        $("#inputLastName").css("border-color", "red");
    } else {
        $("#inputLastName").css("border-color", "rgb(0, 198, 0)");
    }
    if (emailElement == "") {
        errorMessage += "Please fill all mandatory fields<br>"
        $("#inputEmail").css("border-color", "red");
    } else {
        if (!validateEmail(emailElement)) {
            $("#inputEmail").css("border-color", "red");
            errorMessage += "Email is incorrect format<br>"
        } else {
            $("#inputEmail").css("border-color", "rgb(0, 198, 0)");
        }
    }
    if (userNameElement == "") {
        $("#inputUserName").css("border-color", "red");
    } else {
        $("#inputUserName").css("border-color", "rgb(0, 198, 0)");
    }
    if (passwordElement == "") {
        $("#inputPassword").css("border-color", "red");
    } else {
        $("#inputPassword").css("border-color", "rgb(0, 198, 0)");
    }
    if (confirmPasswordElement == "") {
        $("#inputConfirmPassword").css("border-color", "red");
    } else {
        $("#inputConfirmPassword").css("border-color", "rgb(0, 198, 0)");
    }


    if(passwordElement != confirmPasswordElement){
        $("#inputConfirmPassword").css("border-color", "red");
        errorMessage += "Confirm password is not match with password";
    }

    if (errorMessage != "") {
        $('#errorMessage').show();
        $('#errorMessage').html(errorMessage);
        e.preventDefault();
    }

    var userInfos = getCookie('userInfos')
    if (userInfos) {
        userInfos = JSON.parse(userInfos)
    }
    else {
        userInfos = []
    }
    var newUserInfo = [{
        firstName: $('#inputFirstName').val(),
        lastName: $('#inputLastName').val(),
        email: $('#inputEmail').val(),
        userName: $('#inputUserName').val(),
        password: $('#inputPassword').val(),
        confirmPassword: $('#inputConfirmPassword').val()
    }];
    userInfos = userInfos.concat(newUserInfo)
    setCookie('userInfos', JSON.stringify(userInfos), 15)


})

function validateRegister() {
    var errorMessage = ""

    var firstNameElement = $("#inputFirstName");
    var lastNameElement = $("#inputLastName");
    var emailElement = $("#inputEmail");
    var userNameElement = $("#inputUserName");
    var passwordElement = $("#inputPassword");
    var confirmPasswordElement = $("#inputConfirmPassword");

    if (firstNameElement.value() == "" || lastNameElement.value() == "" || emailElement.value() == "" || userNameElement.value == "" || passwordElement == "" || confirmPasswordElement == "") {
        errorMessage += "Please fill all mandatory fields<br>";
    }


    return errorMessage;
}

function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}


function setCookie(cname, cvalue, exdays) {
    var d = new Date(); d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}


function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i].trim();
        if (c.indexOf(name) == 0)
            return c.substring(name.length, c.length);
    } return "";
}
