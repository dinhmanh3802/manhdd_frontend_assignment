$(document).ready(function () {
    setTimeout(function () {
        $('#loading').hide();
        showContents()
    }, 5000)
})

function showContents() {
    $('#content-loading').show()
    var fullcontent = JSON.parse(getCookie('fullcontent'))
    var tbody = $("#contentTable tbody");
    var row = "<tr><td>" + 4 + "</td><td>" + fullcontent.title + "</td><td>" + fullcontent.brief + "</td><td>" + fullcontent.date + "</td></tr>";
    tbody.append(row);
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i].trim();
        if (c.indexOf(name) == 0)
            return c.substring(name.length, c.length);
    } return "";
}