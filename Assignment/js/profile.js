
$(document).ready(function () {
    var profile = JSON.parse(getCookie('profile'))
    $('#userFirstName').val(profile.firstName)
    $('#userLastName').val(profile.lastname)
    $('#userPhoneNumber').val(profile.phoneNumber)
    $('#userDescription').val(profile.description)
})

$('#profile-form').submit(function () {

    var profile = {
        firstName: $('#userFirstName').val(),
        lastname: $('#userLastName').val(),
        phoneNumber: $('#userPhoneNumber').val(),
        description: $('#userDescription').val()
    }
    setCookie('profile', JSON.stringify(profile), 15)
})

function setCookie(cname, cvalue, exdays) {
    var d = new Date(); d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i].trim();
        if (c.indexOf(name) == 0)
            return c.substring(name.length, c.length);
    } return "";
}