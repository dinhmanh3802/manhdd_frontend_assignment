$('#form-content').submit(function () {
    var currentDate = new Date();
    var formattedDate = formatDate(currentDate);

    var fullcontent = {
        title: $('#contentTitle').val(),
        brief: $('#contentBrief').val(),
        content: $('#contentContent').val(),
        date: formattedDate
    };
    setCookie('fullcontent', JSON.stringify(fullcontent),15)
})

function setCookie(cname, cvalue, exdays) {
    var d = new Date(); d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function formatDate(date) {
    var day = date.getDate();
    var month = date.getMonth() + 1; // Months are zero-indexed
    var year = date.getFullYear();
    var hours = date.getHours();
    var minutes = date.getMinutes();

    // Add leading zeros if needed
    day = (day < 10) ? '0' + day : day;
    month = (month < 10) ? '0' + month : month;
    hours = (hours < 10) ? '0' + hours : hours;
    minutes = (minutes < 10) ? '0' + minutes : minutes;

    var formattedDate = day + '/' + month + '/' + year + ' ' + hours + ':' + minutes;
    return formattedDate;
}